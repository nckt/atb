/**
 * Googleマップの設定をします。
 * https://developers.google.com/maps/documentation/javascript/tutorial?hl=ja
 *
 * @author junsuke nakakita
 */
var map;

function initMap(){
  map = new google.maps.Map(
    document.getElementById('googlemap'),
    {
      center : {lat: 35.566810, lng: 139.691756},
      zoom   : 18
    }
  );

  var marker = new google.maps.Marker({
    position : new google.maps.LatLng(35.566810, 139.691756),
    map      : map,
    icon     : "image/map_logo.png"
  });

  google.maps.event.addListener(marker, 'click', (function(url){
    return function(){ window.open(url); };
  })("https://www.google.co.jp/maps/place/%E3%80%92146-0093+%E6%9D%B1%E4%BA%AC%E9%83%BD%E5%A4%A7%E7%94%B0%E5%8C%BA%E7%9F%A2%E5%8F%A3%EF%BC%91%E4%B8%81%E7%9B%AE%EF%BC%92%EF%BC%90%E2%88%92%EF%BC%91+%E3%83%9E%E3%83%86%E3%83%AA%E3%82%A2%E3%83%AB/@35.5668763,139.6907898,18z/data=!4m5!3m4!1s0x6018600a7ccc1be9:0xe75eed3efcefdd5c!8m2!3d35.5668097!4d139.6917556"));

  var mapStyle = [
    {
        "stylers": [
          { "saturation" : -100 },
          { "lightness"  :   -5 }
        ]
    }
  ];
  var mapType = new google.maps.StyledMapType(mapStyle);
  map.mapTypes.set('GrayScaleMap', mapType);
  map.setMapTypeId('GrayScaleMap');
}
