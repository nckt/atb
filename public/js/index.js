/**
 * TOPページで使うJSです。
 * @requires jQuery 3.1.1以上
 */

 $(function() {
  // -------------------------------------------------
  // ページ内リンクをアニメーション移動にする。
  // http://phiary.me/jquery-page-link-smooth-scroll/
  // -------------------------------------------------

  // スクロールのオフセット値
  var offsetY = 0;
  // スクロールにかかる時間
  var time = 400;

  // ページ内リンクのみを取得
  $(".scroll").click(function() {
    var target = $(this.hash);
    if (!target.length) return ;

    var offsetY = parseInt( $(this).attr("offset") ) || 0;
    var targetY = target.offset().top + offsetY;

    $('html,body').animate({scrollTop: targetY}, time, 'swing');
  });

  //--------------------------------------------------------
  // フォントを書き換える(ライセンス上、Cufonを使う必要がある。)
  //--------------------------------------------------------
  var detector = new Detector();
  if( detector.detect("Tw Cen MT") === false ){
    Cufon.replace('.tw-cen-mt', { fontFamily: 'Tw Cen MT', hover: true });
    Cufon.replace('.tw-cen-mt-bold', { fontFamily: 'Tw Cen MT Bold', hover: true });
  }

  //------------------------------------------------------------------
  // 右上のハンバーガーアイコンをクリックするとふわっとメニューが出てくる。
  //------------------------------------------------------------------
  $(".smalldevice_navicon").click(function(){
    $(this).next(".smalldevice_navlist").fadeToggle(200);
  });
});
